#include <cstdio>
#include <iostream>
#include "Radmon.hpp"
#include "TestFixture.hpp"

int
main( int argc, char **argv )
{
	Radmon rm;
	TestFixture tf;

	/* Print all radmon registers
	try {
		rm.print();
	} catch( std::string str ) {
		std::cerr << "Exception: " << str << std::endl;
	}
	*/

	try {
		// Test that registers have default values
		uint16_t control = rm.getControl();
		tf.warn( control == 0x01,
				"Control register should have default value 0x01" );

		tf.warn( rm.getIRQMask() == 0x10,
				"IRQ mask should have default value 0x10" );

		tf.assert( rm.getVersion() == 0x11,
				"Version should be 0x11" );

		tf.warn( rm.getSramPwrCurrentTh() == 0x3ff,
				"sram pwr current threshold should have default value 0x3ff" );

		rm.setControl( 0xff );
		tf.assert( rm.getControl() == 0xff,
				"Control register should keep values set" );
		rm.setControl( control );

		rm.setIRQMask( 0x3f );
		tf.assert( rm.getIRQMask() == 0x3f,
				"IRQ mask should keep values set" );
		rm.setIRQMask( 0x10 );

		rm.setSramPwrCurrentTh( 0x300 );
		rm.getSramPwrCurrentTh();
		tf.assert( rm.getSramPwrCurrentTh() == 0x300,
				"Sram pwr current threshold should keep values set" );
		rm.setSramPwrCurrentTh( 0x3ff );


	} catch( std::string str ) {
		tf.assert( 0, "Caugth exception: `" + str + "' ABORTING" );
	}

	// Print a pretty report and exit failure unless tests pass
	tf.report();
	return tf.passed() ? 0 : 1;
}
