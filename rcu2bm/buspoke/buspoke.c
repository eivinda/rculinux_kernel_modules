#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void
usage(void)
{
	fprintf( stderr, "Usage: buspoke address [value]\n" );
	fprintf( stderr, "  Register is in the range [0x00 .. 0xffff].\n" );
	fprintf( stderr, "  If a value is given it gets written to address.\n" );
	fprintf( stderr, "  The value in the address (after write) is printed to stdout.\n" );
	exit(0);
}


int
main( int argc, char **argv )
{
	uint16_t  addr;
	uint32_t  value = 0xdeadbeef;
	FILE     *f;

	if ( argc < 2 || argc > 3 )
		usage();

	if ( sscanf( argv[1], "%hi", &addr ) != 1 )
		usage();

	if ( argc == 3 && sscanf( argv[2], "%i", &value ) != 1 )
		usage();

	f = fopen( "/dev/rcu2bm", "r+" );
	if ( !f ) {
		perror( "fopen( /dev/rcu2bm )" );
		return 1;
	}

	if ( fseek( f, addr * sizeof(value), SEEK_SET ) != 0 ) {
		perror( "fseek(...)" );
		return 1;
	}
	if ( argc == 3 ) {
		if ( fwrite( &value, sizeof(value), 1, f ) != 1 ) {
			perror( "fwrite(...)" );
			return 1;
		}
		if ( fseek( f, addr * sizeof(value), SEEK_SET ) != 0 ) {
			perror( "fseek(...)" );
			return 1;
		}
	}

	if ( fread( &value, sizeof(value), 1, f ) != 1 ) {
		perror( "fread(...)" );
		return 1;
	}
	printf( "0x%08x\n", value );

	fclose( f );

	return 0;
}
