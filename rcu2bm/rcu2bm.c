#include <asm/uaccess.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/semaphore.h>

MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Eivind Askeland" );

/* Addresses for the bus master interface.
 * Assuming phys == virt address.
 */
static volatile u32* ADDR_REG = (u32*)0x50001000;
static volatile u32* DOUT_REG = (u32*)0x50001004;
static volatile u32* DIN_REG  = (u32*)0x50001008;
static volatile u32* CS_REG   = (u32*)0x5000100c;
static volatile u32* MODE_REG = (u32*)0x50001020;

static const char name[] = "rcu2bm";

static struct {
	dev_t              devt;
	struct cdev        cdev;
	struct class      *class;
	struct semaphore   lock;
} rcu2bm;


static inline u32
read( u16 addr )
{
	u32 value;
	down( &rcu2bm.lock );
	//printk( KERN_INFO "%s reading from address %hu\n", name, addr );

	*ADDR_REG = addr;
	do {
		*CS_REG = 0b10;
	} while ( !(*CS_REG & 0b100) );
	value = *DIN_REG;

	up( &rcu2bm.lock );
	return value;
}

static inline void
write( u16 addr, u32 value )
{
	down( &rcu2bm.lock );
	//printk( KERN_INFO "%s writing %u to address %hu\n", name, value, addr );

	*ADDR_REG = addr;
	*DOUT_REG = value;
	do {
		*CS_REG = 0b11;
	} while ( !(*CS_REG & 0b100) );

	up( &rcu2bm.lock );
}


/*
 * File opperations for rcu2bm char-device.
 */
static int
rcu2bm_open( struct inode *inode, struct file *file )
{
	file->f_pos = 0;
	return 0;
}


static ssize_t
rcu2bm_read( struct file *file, char __user *user_buff, size_t len, loff_t *off )
{
	u16 addr;
	u32 val;

	if ( *off & 0b11 ) {
		/* Reject unnaligned reads */
		return -EINVAL;
	}
	addr = *off >> 2;
	val = read( addr );
	if ( len > sizeof(val) )
		len = sizeof(val);
	len -= copy_to_user( user_buff, &val, len );
	*off += len;
	return len;
}


static ssize_t
rcu2bm_write( struct file *file, const char __user *user_buff, size_t len, loff_t *off )
{
	u16 addr;
	u32 val;
	if ( *off & 0b11 ) {
		/* Reject unnaligned writes */
		return -EINVAL;
	}
	addr = *off >> 2;
	if ( len > sizeof(val) )
		len = sizeof(val);
	if ( copy_from_user( &val, user_buff, len ) )
		return 0;
	write( addr, val );
	*off += len;
	return len;
}


static loff_t
rcu2bm_seek( struct file *file, loff_t off, int whence )
{
	loff_t target;
	switch( whence ) {
	case SEEK_SET :
		target = off;
		break;
	case SEEK_CUR :
		target = file->f_pos + off;
		break;
	case SEEK_END :
		target = 0xffff + off;
		break;
	default :
		return -EINVAL;
	}
	if ( target < 0 )
		return -EINVAL;
	file->f_pos = target;
	return target;
}


static const struct file_operations rcu2bm_fops = {
	.owner = THIS_MODULE,
	.read = rcu2bm_read,
	.open = rcu2bm_open,
	.write = rcu2bm_write,
	.llseek = rcu2bm_seek,
};


static int __init
rcu2bm_init_chardev(void)
{
	int err;
	printk( KERN_INFO "Hello, %s reporting in\n", name );
	rcu2bm.devt = MKDEV( 0, 0 );
	err = alloc_chrdev_region( &rcu2bm.devt, 0, 1, name );
	if ( err < 0 ) {
		printk( KERN_ALERT "Failed to create device: %d.\n", err );
		return err;
	}

	cdev_init( &rcu2bm.cdev, &rcu2bm_fops );
	rcu2bm.cdev.owner = THIS_MODULE;
	rcu2bm.cdev.ops = &rcu2bm_fops;

	rcu2bm.class = class_create( THIS_MODULE, name );
	if ( !rcu2bm.class ) {
		printk( KERN_ALERT "Failed to create class.\n" );
		err = -1;
		goto err1;
	}
	if ( !device_create( rcu2bm.class, NULL, rcu2bm.devt, NULL, name ) ) {
		printk( KERN_ALERT "Failed to create device.\n" );
		err = -1;
		goto err2;
	}

	err = cdev_add( &rcu2bm.cdev, rcu2bm.devt, 1 );
	if ( err < 0 ) {
		printk( KERN_ALERT "Failed to add device: %d.\n", err );
		goto err3;
	}

	return 0;

err3:
	device_destroy( rcu2bm.class, rcu2bm.devt );
err2:
	class_destroy( rcu2bm.class );
err1:
	unregister_chrdev_region( rcu2bm.devt, 1 );
	return err;
}


static void __exit
rcu2bm_rm_chardev(void)
{
	printk( KERN_INFO "Goodbye from %s\n", name );
	cdev_del( &rcu2bm.cdev );
	device_destroy( rcu2bm.class, rcu2bm.devt );
	class_destroy( rcu2bm.class );
	unregister_chrdev_region( rcu2bm.devt, 1 );
}


static int __init
rcu2bm_init( void )
{
	sema_init( &rcu2bm.lock, 1 );
	*CS_REG = 0b10000; /* Reset */
	*MODE_REG = 1;     /* Command mode */
	return rcu2bm_init_chardev();
}


static void __exit
rcu2bm_exit( void )
{
	rcu2bm_rm_chardev();
}


module_init( rcu2bm_init );
module_exit( rcu2bm_exit );
